import React, { Component } from 'react';
import {Container, Header, Content, Text, Item, Icon, Input, Button} from 'native-base';
import {Grid, Row, Col} from 'react-native-easy-grid';

class GridNew extends Component {
constructor(){
    super();
    this.arr = [];
}
componentWillMount(){
    for(let i = 1; i<=10;i++){
        this.arr.push(i);
    }
}
printData(){
    return this.arr.map((item) => {
        return (
            <Row style={{borderRadius:50, overflow:'hidden', marginTop:20}} key={item}>
                  <Col style={{width:130, backgroundColor:'skyblue'}}></Col>
                  <Col style={{padding:20, backgroundColor:'white'}}>
                    <Text style={{fontSize:25}}>Community</Text>
                    <Text>1234567</Text>
                    <Text>Explore Now</Text>
                  </Col>
              </Row>
        )
    })
}
  render() {
    return (
      <Container style={{backgroundColor:'#c2e8ff'}}>
          <Header searchBar rounded>
            <Item>
                <Icon name="ios-search" />
                <Input placeholder="Search" />
                <Icon name="ios-people" />
            </Item>
            <Button transparent>
                <Text>Search</Text>
            </Button>
        </Header>
          <Content padder>
              {this.printData()}
          </Content>
      </Container>
    );
  }
}

export default GridNew;
