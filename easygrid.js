import React, { Component } from 'react';
import {Container, Content, Header, Text} from 'native-base';
import {Grid, Row, Col} from 'react-native-easy-grid';
class easygrid extends Component {
  render() {
    return (
      <Container>
          <Header></Header>
            <Grid>
                <Row style={{backgroundColor:'red'}}>
                    <Col style={{backgroundColor:'yellow'}}></Col>
                    <Col>
                        <Row style={{backgroundColor:'gray'}}></Row>
                        <Row style={{backgroundColor:'pink'}}></Row>
                    </Col>
                </Row>
                
            </Grid>
      </Container>
    );
  }
}

export default easygrid;
