import React, { Component } from 'react';
import { Container, Content, Text,View, Header, Input, Item, Label , Button} from 'native-base';


class mycode extends Component {
  constructor(props) {
    super(props);
    this.state = {
        first:'',
        second:'',
        third:'',
        fourth:'',
        fifth:'',
        six:'',
        name:'',
        domain:''
    };
  }
  generateEmails(){
      let arr = this.state.name.split(' ');
      let first = arr[0] + "@" + this.state.domain;
      first = first.toLowerCase();
      let second = arr[0][0] + arr[1] + "@" + this.state.domain;
      second = second.toLowerCase();
      this.setState({first:first,second:second});
  }
  render() {
    
    return (
      <Container>
          <Header
            style={{backgroundColor:'#000000'}}
          />
          <Content padder>
              <Item floatingLabel>
                  <Label>Enter Name</Label>
                  <Input 
                    onChangeText={(t) => this.setState({name:t})}
                  />
              </Item>
              <Item floatingLabel>
                  <Label>Enter Domain</Label>
                  <Input 
                    onChangeText={(t) => this.setState({domain:t})}
                  />
              </Item>
              <Button style={{justifyContent:'center', marginTop:30}}
                onPress={() => this.generateEmails()}
              >
                  <Text>Click ME</Text>
              </Button>
              <View style={{padding:15}}>
                <Text style={{fontWeight:'bold'}}>First Email</Text>
                <Text>{this.state.first}</Text>
              </View>
              <View style={{padding:15}}>
                <Text style={{fontWeight:'bold'}}>Second Email</Text>
                <Text>{this.state.second}</Text>
              </View>
              <View style={{padding:15}}>
                <Text style={{fontWeight:'bold'}}>Third Email</Text>
                <Text>{this.state.third}</Text>
              </View>
              <View style={{padding:15}}>
                <Text style={{fontWeight:'bold'}}>Fourth Email</Text>
                <Text>{this.state.fourth}</Text>
              </View>
              <View style={{padding:15}}>
                <Text style={{fontWeight:'bold'}}>Fifth Email</Text>
                <Text>{this.state.fifth}</Text>
              </View>
              <View style={{padding:15}}>
                <Text style={{fontWeight:'bold'}}>Six Email</Text>
                <Text>{this.state.six}</Text>
              </View>
          </Content>
      </Container>
    );
  }
}

export default mycode;
