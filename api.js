import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Grid, Row, Col} from 'react-native-easy-grid';
import {Container, Content, Header } from 'native-base';
class api extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data:[]
    };
  }
  fetchData(){
    fetch('https://restcountries.eu/rest/v2/all')
    .then((data) => data.json())
    .then((res) => {
        this.setState({data:res});
    })
  }
  componentWillMount(){
      this.fetchData();
  }
  render() {
    return (
        <Container>
            <Header></Header>
            <Content>
                {this.state.data.map((item) => {
                    return(
                        <Text style={{padding:15, borderColor:'red', borderWidth:2}}>{item.name}</Text>
                    )
                })}
            </Content>
        </Container>
    );
  }
}

export default api;
