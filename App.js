import React from 'react';
import {Container,Text, Header, Content, Left, Right, Body, Form, Item, Label, Input, Button, Icon, Card, CardItem, View} from 'native-base';
import mycode from './mycode';
import GridNew from './gridnew';
import api from './api';
import weather from './weather';
import {createAppContainer, createSwitchNavigator , createDrawerNavigator} from 'react-navigation';
class App extends React.Component{
  constructor(){
    super();
    this.state = {username:'', password:''};
  }
  login(){
    if(this.state.username == this.state.password){
     this.props.navigation.navigate('weather');
    }else{
      alert('Error');
    }
  }
  render(){
    return(
      <Container>
        <Header>
          <Left><Text>L</Text></Left>
          <Body><Text>Body</Text></Body>
          <Right><Text>R</Text></Right>
        </Header>
        <Content padder>
          <Text style={{fontSize:50, textAlign:'center'}}>
            Login Page
          </Text>
          <Form>
            <Item floatingLabel>
              <Label>Username</Label>
              <Input 
                onChangeText={(t) => this.setState({username:t})}
              />
            </Item>  
            <Item floatingLabel>
              <Label>Password</Label>
              <Input 
                secureTextEntry
                onChangeText={(t) => this.setState({password:t})}
              />
            </Item>  
            {/* success | info | warning | danger */}
            
          </Form>
          <Button success rounded bordered 
            style={{marginTop:30, justifyContent:'center'}}
            onPress={() => this.login()}
          >
            {/* npm install react-native-vector-icons
            react-native link react-native-vector-icons */}
            <Icon name="folder" />
            <Text style={{textAlign:'center'}}>Login</Text>
          </Button>
          <Card>
            <CardItem header bordered>
              <Text>Header</Text>
            </CardItem>
            <CardItem>
              <Body>
                <View style={{width:100, height:100, backgroundColor:'green', borderRadius:50, alignSelf:'center'}}></View>
                <Text>This is my card body.</Text>
              </Body>
            </CardItem>
            <CardItem footer bordered>
              <Text>Card Footer</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    )
  }
}

const navigation = createSwitchNavigator({
  App,
  weather,
  GridNew,
  api,
  mycode
})

export default createAppContainer(navigation);